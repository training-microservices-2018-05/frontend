package com.artivisi.training.microservice201805.frontend.controller;

import com.artivisi.training.microservice201805.frontend.service.CatalogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
    @Autowired private CatalogService catalogService;

    @GetMapping("/product/list")
    public ModelMap daftarProduct() {
        LOGGER.info("Menampilkan daftar produk");
        return new ModelMap()
                .addAttribute("productData", catalogService.dataProduct());
    }
}
